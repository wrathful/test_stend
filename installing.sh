#!/bin/bash
sudo apt install python3.6
sudo apt install python3-pip
sudo pip3 install keras
sudo apt-get install python3-opencv
sudo pip3 install theano
sed -i 's/tensorflow/theano/' ~/.keras/keras.json
sudo pip3 install numpy
sudo pip3 install h5py
sudo pip3 install pandas
sudo pip3 install pillow
