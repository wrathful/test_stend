import cv2
import numpy as np
import os,shutil, random
from PIL import Image
from keras import backend as K
from keras.models import load_model
from keras import applications
from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D
from keras.models import Model
class web_numbers:
    def __init__(self):
        self.count=0
        model = applications.mobilenet.MobileNet(include_top=False, weights=None, input_shape=(128, 128, 3))
        x = model.output
        x = GlobalAveragePooling2D()(x)
        x = Dropout(0.5)(x)
        x = Dense(64, activation="relu")(x)
        x = Dropout(0.5)(x)
        predictions_gender = Dense(10, activation="softmax")(x)
        model_final = Model(inputs=model.input, outputs=predictions_gender)
        model_final.load_weights('model_numbers.hdf5')
        self.model = model_final
    
    def start(self):
        def load_image(img_path_):
           image_ = cv2.imread(img_path_)
           image_ = cv2.cvtColor(image_, cv2.COLOR_BGR2RGB)
           image_ = cv2.resize(image_, (128, 128))
           image_ = image_/255
           image_ = np.expand_dims(image_, 0)
           return image_
        while True:
           print("Чтобы сделать снимок с веб-камеры нажмите enter, вернутся в меню - 0")
           name = input()
           if name=="0":
               return
           cap = cv2.VideoCapture(0)
           ret, frame = cap.read()
           
           cv2.imwrite('cam.png', frame)
           img = cv2.imread('cam.png')
           gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY) # меняем цветовую модель с BGR на HSV 
           #print(gray.shape)
           ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU) # применяем цветовой фильтр
           kernel = np.ones((5,5), np.uint8)
           img_dilation = cv2.dilate(thresh, kernel, iterations=1)
#find contours
           im2,ctrs, hier = cv2.findContours(img_dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
           numbers=[]
#sort contours
           sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
    
           for i, ctr in enumerate(sorted_ctrs):
               # Get bounding box
               x, y, w, h = cv2.boundingRect(ctr)
               # Getting ROI
               roi = img[y:y+h, x:x+w]        
               # show ROI
               if w>15 and h>25 and w<200 and h<200:
                  if np.min(roi) == 0:
                      numbers.append(roi)

           result=''
           for i in range(len(numbers)):
               image=cv2.resize(numbers[i], (128, 128))              
               ret,gray1 = cv2.threshold(numbers[i],127,255,cv2.THRESH_BINARY)
               gray1 = cv2.cvtColor(numbers[i],cv2.COLOR_BGR2GRAY)
               cv2.imwrite('cam.png', gray1)
               loadimg=load_image('cam.png')               
               y=self.model.predict(loadimg)               
               result=result+str(np.argmax(y[0]))                    
           print(result)
           cap.release()



   
